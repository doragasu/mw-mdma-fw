/*
 * Megadrive standard mapper for s29gl032 and similar memory chips. This works
 * sending commands directly to the memory chip, as no mapper is needed.
 */

#include <stdbool.h>
#include <util/delay.h>

#include "flash_drv.h"
#include "mdma-pr.h"
#include "cart_if.h"
#include "util.h"

#define BUF_BITS 4
#define BUF_WLEN (1<<BUF_BITS)
#define BUF_MASK (BUF_WLEN - 1)

/// Base address for the CFI data
#define CFI_BASE 0x10

// Offset in the CFI data buffer
#define CFI_LENGTH_OFF (0x27 - CFI_BASE)
#define CFI_NUM_REGIONS_OFF (0x2C - CFI_BASE)
#define CFI_REGION_DATA_OFF (0x2D - CFI_BASE)


static struct flash_layout flash = {};
static uint8_t man_id = 0xFF;
static uint8_t dev_id[3] = { 0xFF, 0xFF, 0xFF };

static uint16_t bswap16(uint16_t word)
{
	return ((word)>>8) | ((word)<<8);
}

static void bus_write(uint32_t addr, uint16_t data)
{
	// Put address on the bus
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = addr>>8;
	CIF_ADDRU_PORT = (CIF_ADDRU_PORT & (~CIF_ADDRU_MASK)) |
		((addr>>16) & CIF_ADDRU_MASK);
	// Write data to bus
	CIF_DATAL_PORT = data;
	CIF_DATAH_PORT = data>>8;
	CIF_DATAH_DDR = CIF_DATAL_DDR = 0xFF;
	// Select chip
	CIF_CLR__CE;
	// Signal _W
	CIF_CLR__W;

	// Disable _W
	CIF_SET__W;
	// Remove data from bus
	CIF_DATAH_DDR  = CIF_DATAL_DDR  = 0;
	CIF_DATAH_PORT = CIF_DATAL_PORT = 0xFF;
	// Deselect chip
	CIF_SET__CE;
}

// Version of the function above, but using 16-bit addr and 8-bit data
// Useful for sending commands to the flash chip wasting less CPU cycles
static void bus_short_write(uint16_t addr, uint8_t data)
{
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = addr>>8;

	CIF_DATAL_PORT = data;
	CIF_DATAL_DDR = 0xFF;
	CIF_CLR__CE;
	CIF_CLR__W;

	CIF_SET__W;
	CIF_DATAL_DDR  = 0;
	CIF_DATAL_PORT = 0xFF;
	CIF_SET__CE;
}

static uint16_t bus_read(uint32_t addr)
{
	uint16_t data;

	// Put address on the bus
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = addr>>8;
	CIF_ADDRU_PORT = (CIF_ADDRU_PORT & (~CIF_ADDRU_MASK)) |
		((addr>>16) & CIF_ADDRU_MASK);
	// Enable chip outputs
	CIF_CLR__OE;
	// Select chip
	CIF_CLR__CE;
	// Read data
	_NOP(); // Insert NOPs to ensure the input sinchronizer gets the data
	_NOP();
	data = (((uint16_t)CIF_DATAH_PIN)<<8) | CIF_DATAL_PIN;
	// Deselect chip
	CIF_SET__CE;
	// Disable chip outputs
	CIF_SET__OE;

	return data;
}

static void unlock(void)
{
	bus_short_write(0x555, 0xAA);
	bus_short_write(0x2AA, 0x55);
}

static void reset(void)
{
	bus_short_write(0, 0xF0);
}

// buf must be 45 byte long or greater
static void cfi_query(uint8_t *buf)
{
	// NOTE: all chips I have seen accept the CFI Query command while in
	// autoselect mode, but entering autoselect is not required.
	bus_short_write(0x55, 0x98);

	// Read 45 bytes from CFI_BASE
	for (uint8_t i = 0, addr = CFI_BASE; i < 45; i++, addr++) {
		buf[i] = bus_read(addr);
	}

	// Exit CFI Query
	reset();
}

static int8_t metadata_populate(const uint8_t *cfi)
{
	flash.len = 1LU<<cfi[CFI_LENGTH_OFF];
	flash.num_regions = cfi[CFI_NUM_REGIONS_OFF];

	uint32_t start_addr = 0;
	const uint8_t *cfi_reg = cfi + CFI_REGION_DATA_OFF;
	for (uint8_t i = 0; i < flash.num_regions; i++) {
		struct flash_region *reg = &flash.region[i];

		reg->start_addr = start_addr;
		reg->num_sectors = *cfi_reg++ + 1;;
		reg->num_sectors |= 256 * *cfi_reg++;
		reg->sector_len = *cfi_reg++;
		reg->sector_len |= 256 * *cfi_reg++;

		start_addr += ((uint32_t)reg->sector_len * 256) * reg->num_sectors;
	}

	// The end of computed sector lengths should match the flash length
	if (start_addr != flash.len) {
		return -1;
	}

	return 0;
}

static int8_t data_poll(uint32_t addr, uint8_t data) {
	uint8_t readed;

	// Poll while DQ7 != data(7) and DQ5 == 0 and DQ1 == 0
	do {
		readed = bus_read(addr);
	} while (((data ^ readed) & 0x80) && ((readed & 0x20) == 0));

	// DQ7 must be tested after another read, according to datasheet
	readed = bus_read(addr);
	if (((data ^ readed) & 0x80) == 0) return 0;
	// Data has not been programmed, return with error. If DQ5 is set, also
	// issue a reset command to return to array read mode
	if (readed & 0x20) {
		reset();
	}
	return 1;
}

static void hw_reset(void)
{
	// Hold reset during at least 500 ns (4 cycles@8MHz)
	CIF_CLR__RST;
	_NOP();_NOP();_NOP();_NOP();
	// Remove reset condition from flash chip
	CIF_SET__RST;
	// Wait 500 ns
	_NOP();_NOP();_NOP();_NOP();

	// Delay 1 ms for chips to get ready to accept commands
	_delay_ms(1.0);
}

static int8_t flash_chip_init(void)
{
	// Check for read CFI data
	static const uint8_t cfi_check[] = {
		0x51, 0x52, 0x59, 0x02, 0x00
	};
	// Buffer for CFI flash chip data
	uint8_t cfi[45];

	// Reset chip
	hw_reset();

	// Read CFI data
	cfi_query(cfi);

	// Check response contains QRY and algorithm is AMD compatible
	for (uint16_t i = 0; i < sizeof(cfi_check); i++) {
		if (cfi[i] != cfi_check[i]) {
			// No flash chip installed, or chip not supported
			return -1;
		}
	}

	// Check number of regions is coherent and populate metadata
	const uint8_t num_regions = cfi[CFI_NUM_REGIONS_OFF];
	if (!num_regions || num_regions > FLASH_REGION_MAX) {
		return -1;
	}

	return metadata_populate(cfi);
}

static void ids_read(void)
{
	unlock();
	bus_short_write(0x5555, 0x90); // Autoselect
	// Note: keeping only lower byte of manufactured and device IDs
	man_id = bus_read(0x00);
	// Although it is not completely clear on the datasheet if dev_id can be
	// read after the man_id without exiting and reentering autoselect mode,
	// it has been tested and it works.
	dev_id[0] = bus_read(0x01);
	dev_id[1] = bus_read(0x0E);
	dev_id[2] = bus_read(0x0F);
	reset();
}

static int8_t init(void)
{
	int8_t err = flash_chip_init();
	if (err) {
		return -1;
	}

	ids_read();

	return 0;
}

static void flash_idle(void)
{
	CIF_SET__W;
	CIF_SET__OE;
	CIF_SET__CE;
	CIF_DATAH_DDR  = CIF_DATAL_DDR = 0;
	CIF_DATAH_PORT = CIF_DATAL_PORT = 0xFF;
	CIF_ADDRH_PORT = CIF_ADDRL_PORT = 0xFF;
	CIF_ADDRU_PORT |= CIF_ADDRU_MASK;
}

static void deinit(void)
{
	flash_idle();
}

static uint8_t man_id_get(void)
{
	return man_id;
}

static const uint8_t* dev_id_get(uint8_t *id_len)
{
	*id_len = 3;
	return dev_id;
}

static const struct flash_layout *layout_get(void)
{
	return &flash;
}

static int16_t read(uint32_t addr, uint8_t *data, uint16_t len)
{
	// Convert length and address from bytes to words
	uint32_t waddr = addr >> 1;
	uint16_t wlen = len >> 1;
	uint16_t *wbuf = (uint16_t*)data;

	for (uint16_t i = 0; i < wlen; i++, waddr++) {
		wbuf[i] = bswap16(bus_read(waddr));
	}

	return wlen<<1;
}

// Writes a buffer, but does **not** poll until data is written.
// Polling must be done outside this function.
static void buf_write(uint32_t waddr, const uint16_t *buf, uint8_t wlen)
{
	uint32_t sa = waddr;

	// Unlock and command write to buffer
	unlock();
	bus_write(sa, 0x25);
	// Write word length minus 1
	bus_write(sa, wlen - 1);

	// Write words to buffer
	while (wlen--) {
		bus_write(waddr++, bswap16(*buf++));
	}

	// Start buffer programming to flash
	bus_write(sa, 0x29);
}

static int16_t program(uint32_t addr, const uint8_t *data, uint16_t len)
{
	// Convert length and address from bytes to words
	uint16_t wlen = len >> 1;
	uint32_t waddr = addr >> 1;
	const uint16_t *wbuf = (const uint16_t*)data;
	// Address to read within current buffer
	const uint8_t buf_addr = waddr & BUF_MASK;
	// Words to write on each iteration, less or equal than buffer length
	uint8_t to_write = MIN(len, BUF_WLEN - buf_addr);
	uint16_t written = 0;
	bool err = false;

	while (!err && to_write) {
		buf_write(waddr, wbuf, to_write);
		// Prepare to write next buffer
		wlen -= to_write;
		wbuf += to_write;
		written += to_write;
		waddr += to_write;
		to_write = MIN(wlen, BUF_WLEN);
		// Poll until data is written
		err = data_poll(waddr - 1, (*(wbuf - 1))>>8);
	}

	return written<<1;
}

static void erase_unlock(void)
{
	unlock();
	bus_short_write(0x555, 0x80);
	bus_short_write(0x555, 0xAA);
	bus_short_write(0x2AA, 0x55);
}

static int8_t chip_erase(void)
{
	erase_unlock();
	bus_short_write(0x555, 0x10);
	return data_poll(0, 0xFF);
}

static int8_t flash_sector_limits(uint32_t addr, uint32_t *start, uint32_t *next)
{
	if (addr >= flash.len) {
		// Requested address does not fit in chip
		return -1;
	}

	// Find in which region is the address
	uint16_t reg_num = flash.num_regions - 1;
	while (addr < flash.region[reg_num].start_addr) {
		reg_num--;
	}

	// Compute limits
	const struct flash_region *reg = &flash.region[reg_num];
	const uint32_t sect_len = ((uint32_t)reg->sector_len) * 256;
	const uint32_t base = addr - reg->start_addr;
	const uint32_t trim = (base & ~(sect_len - 1)) + reg->start_addr;
	if (start) {
		*start = trim;
	}
	if (next) {
		*next = trim + sect_len;
	}

	return 0;
}

static uint32_t sector_erase(uint32_t addr)
{
	const uint32_t waddr = addr>>1;
	uint32_t next = 0;

	erase_unlock();
	bus_write(waddr, 0x30);
	// Compute next sector address while erase is in progress
	flash_sector_limits(addr, NULL, &next);
	const int8_t err = data_poll(waddr, 0xFF);
	if (err) {
		return 0;
	}

	return next;
}

// Register MegaWiFi compatible driver
FLASH_DRV_REGISTER(md_s29, MDMA_CART_TYPE_MEGAWIFI)
