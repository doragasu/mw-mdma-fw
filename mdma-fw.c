/** \file
 *
 *  \brief Main source file for the MegaDrive Memory Administration firmware.
 *  This firmware runs on the MeGaWiFi Programmer board by doragasu.
 *
 *  \author doragasu
 *  \date   2015
 */

#define  INCLUDE_FROM_BULKVENDOR_C
#include "mdma-fw.h"
#include "cart_if.h"
#include "util.h"
#include "sys_fsm.h"
#include "bloader.h"
#include "wifi-if.h"

int main(void)
{
	uint8_t buttonStat, prevButtonStat;

	// Init LUFA related stuff
	SetupHardware();
	// If button pressed, enter bootloader
	if (Buttons_GetStatus()) {
		JumpToBootloader();	
	}
	// Init Cartridge interface (and leave it in reset state)
	CifInit();
	CIF_SET__RST;
	// Init system state machine
	sf_init();
	// Initialize WiFi chip interface
	WiFiInit();

	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	prevButtonStat = Buttons_GetStatus();
	GlobalInterruptEnable();

	while (true) {
		USB_USBTask();
		// If button changed status, send event
		if ((buttonStat = Buttons_GetStatus()) != prevButtonStat) {
			sf_fsm_cycle(buttonStat?SF_EVT_SW_PRESS:SF_EVT_SW_REL);
			prevButtonStat = buttonStat;
		}

		Endpoint_SelectEndpoint(VENDOR_OUT_EPADDR);
		if (Endpoint_IsOUTReceived())
		{
			LEDs_TurnOnLEDs(LEDMASK_USB_BUSY);
			sf_fsm_cycle(SF_EVT_DIN);
			LEDs_TurnOffLEDs(LEDMASK_USB_BUSY);
		}
	}
}

void SetupHardware(void)
{
#if (ARCH == ARCH_AVR8)
	// Disable watchdog if enabled by bootloader/fuses
	MCUSR &= ~(1 << WDRF);
	wdt_disable();

	// Disable clock division
	clock_prescale_set(clock_div_1);
#elif (ARCH == ARCH_XMEGA)
	// Start the PLL to multiply the 2MHz RC oscillator to 32MHz and switch
	// the CPU core to run from it
	XMEGACLK_StartPLL(CLOCK_SRC_INT_RC2MHZ, 2000000, F_CPU);
	XMEGACLK_SetCPUClockSource(CLOCK_SRC_PLL);

	// Start the 32MHz internal RC oscillator and start the DFLL to increase
	// it to 48MHz using the USB SOF as a reference
	XMEGACLK_StartInternalOscillator(CLOCK_SRC_INT_RC32MHZ);
	XMEGACLK_StartDFLL(CLOCK_SRC_INT_RC32MHZ, DFLL_REF_INT_USBSOF, F_USB);

	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
#endif

	// Hardware Initialization
	LEDs_Init();
	Buttons_Init();
	USB_Init();
}

/** \brief Event handler for the USB_Connect event. This indicates that the
 *  device is enumerating via the status LEDs.
 */
void EVENT_USB_Device_Connect(void)
{
	/* Indicate USB enumerating */
	LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** \brief Event handler for the USB_Disconnect event. This indicates that
 *  the device is no longer connected to a host via the status LEDs.
 */
void EVENT_USB_Device_Disconnect(void)
{
	/* Indicate USB not ready */
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	sf_fsm_cycle(SF_EVT_USB_DET);
}

/** \brief Event handler for the USB_ConfigurationChanged event. This is fired
 *  when the host set the current configuration of the USB device after
 *  enumeration - the device endpoints are configured.
 */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool cfg_success = true;

	/* Setup Vendor Data Endpoints */
	cfg_success &= Endpoint_ConfigureEndpoint(VENDOR_IN_EPADDR,
			EP_TYPE_BULK, VENDOR_I_EPSIZE, 2);
	cfg_success &= Endpoint_ConfigureEndpoint(VENDOR_OUT_EPADDR,
			EP_TYPE_BULK, VENDOR_O_EPSIZE, 2);

	// Set LEDs and generate FSM events according to result
	if (cfg_success) {
		LEDs_SetAllLEDs(LEDMASK_USB_READY);
		sf_fsm_cycle(SF_EVT_USB_ATT);
	} else {
		LEDs_SetAllLEDs(LEDMASK_USB_ERROR);
		sf_fsm_cycle(SF_EVT_USB_ERR);
	}
}

/** \brief Event handler for the USB_ControlRequest event. This is used to
 *  catch and process control requests sent to the device from the USB host
 *  before passing along unhandled control requests to the library for
 *  processing internally.
 */
void EVENT_USB_Device_ControlRequest(void)
{
	// Process vendor specific control requests here
}
