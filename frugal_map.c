/*
 * Frugal Mapper driver. Frugal mapper is a SMS mapper using sst39sf040 chip,
 * with the initial 32 KiB fixed at the beginning of the flash, and the slot 2
 * mappable in 16 KiB blocks by writing the bank to the SRAM mirror range
 * ($E000 ~ $FFFF)
 */

#include <stdbool.h>

#include "flash_drv.h"
#include "cart_if.h"
#include "mdma-pr.h"
#include "util.h"

#define CHIP_LEN (512 * 1024LU)

// Mapper blocks are 16 KiB long (14 bits)
#define BLOCK_NBITS 14
#define BLOCK_LEN (1<<BLOCK_NBITS)
#define BLOCK_MASK (BLOCK_LEN - 1)

// Flash sectors are 4 KiB long (12 bits)
#define SECT_NBITS 12
#define SECT_LEN (1<<SECT_NBITS)
#define SECT_MASK (SECT_LEN - 1)

#define BLOCK(addr) ((addr)>>BLOCK_NBITS)

static uint8_t man_id = 0xFF;
static uint8_t dev_id = 0xFF;
static uint8_t block_shadow;

static void bus_addr_logic_update(uint16_t addr)
{
	const bool m8_b = (addr & 0xC000) != 0x8000;
	const bool mc_f = (addr & 0xC000) != 0xC000;

	// M8_B is connected to #TIME MD pin
	if (m8_b) {
		CIF_SET__TIME;
	} else {
		CIF_CLR__TIME;
	}

	// MC_F is connected to #RST MD pin
	if (mc_f) {
		CIF_SET__RST;
	} else {
		CIF_CLR__RST;
	}
}

static void bus_addr_logic_release(void)
{
	CIF_SET__TIME;
	CIF_SET__RST;
}

static void bus_write(uint16_t addr, uint8_t data) {
	// Put address on the bus
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = addr>>8;
	// Update logic addressing pins
	// TODO Updating these pins is only needed to access the bank register.
	// A faster version of the function could be done without updating it
	// by ensuring they have appropriate values:
	// 1. #M8-B set to 1 to ensure Q0 = 0
	// 2. #MC-F set to 1 to avoid loading the bank register
	bus_addr_logic_update(addr);

	// Write data to bus
	CIF_DATAL_PORT = data;
	CIF_DATAL_DDR = 0xFF;
	// Select chip
	CIF_CLR__CE;
	// Signal _W
	CIF_CLR__W;

	// At this time, data is stable at port

	// Disable _W
	CIF_SET__W;
	// Release logic addressing pins
	bus_addr_logic_release();
	// Remove data from bus
	CIF_DATAL_DDR  = 0;
	CIF_DATAL_PORT = 0xFF;
	// Deselect chip
	CIF_SET__CE;
}

static uint8_t bus_read(uint16_t addr) {
	uint8_t data;

	// Put address on the bus
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = addr>>8;
	// Update logic addressing pins
	bus_addr_logic_update(addr);

	// Select chip
	CIF_CLR__CE;
	// Enable chip outputs
	CIF_CLR__OE;
	// Read data
	_NOP();		// Insert NOPs to ensure the input sinchronizer gets the data
	_NOP();
	//while (PINB & 0x80);	// For debugging reads
	data = CIF_DATAL_PIN;
	// Deselect chip
	CIF_SET__CE;
	// Disable chip outputs
	CIF_SET__OE;
	// Release logic addressing pins
	bus_addr_logic_release();
	
	return data;
}

static void block_map(uint8_t block)
{
	if (block != block_shadow) {
		bus_write(0xFFFF, block);
		block_shadow = block;
	}
}

static void unlock(void)
{
	bus_write(0x5555, 0xAA);
	bus_write(0x2AAA, 0x55);
}

static void reset(void)
{
	bus_write(0, 0xF0);
}

static void ids_read(void)
{
	unlock();
	bus_write(0x5555, 0x90);
	// Software id requires 150 ns max to enter
	_NOP();
	_NOP();
	man_id = bus_read(0);
	dev_id = bus_read(1);
	reset();
}

static int8_t init(void)
{
	// To force mapper init to be performed
	block_shadow = 0xFF;
	block_map(0);

	ids_read();

	return 0;
}

static void flash_idle(void)
{
	CIF_SET__W;
	CIF_SET__OE;
	CIF_SET__CE;
	CIF_DATAL_DDR = 0;
	CIF_DATAL_PORT = 0xFF;
	CIF_ADDRH_PORT  = CIF_ADDRL_PORT = 0xFF;
}

static void deinit(void)
{
	flash_idle();
}

static uint8_t man_id_get(void)
{
	return man_id;
}

static const uint8_t *dev_id_get(uint8_t *id_len)
{
	*id_len = 1;
	return &dev_id;
}

// Chips used by this driver do not support CFI, thus assume a uniform
// 512 KiB layout with 4 KiB sectors
static const struct flash_layout *layout_get(void)
{
	static const struct flash_layout layout = {
		.len = CHIP_LEN,
		.num_regions = 1,
		.region[0] = {
			.start_addr = 0,
			.num_sectors = CHIP_LEN / SECT_LEN,
			// Sector length is in 256 byte units
			.sector_len = SECT_LEN / 256
		}
	};

	return &layout;
}

// Read data from current block. block_addr and len must NOT cross
// block boundaries
static void block_read(uint16_t block_addr, uint8_t *buf, uint16_t len)
{
	block_addr |= 0x8000;
	while (len--) {
		*buf++ = bus_read(block_addr++);
	}
}

static int16_t read(uint32_t addr, uint8_t *data, uint16_t len)
{
	// Current block
	uint8_t cur_block = BLOCK(addr);
	// Address to read within current block
	uint16_t block_addr = addr & BLOCK_MASK;
	// Bytes to read on each iteration, less or equal than block length
	uint16_t to_read = MIN(len, BLOCK_LEN - block_addr);
	uint16_t readed = 0;

	while (to_read) {
		block_map(cur_block);
		block_read(block_addr, data, to_read);
		// Move to next block
		cur_block++;
		block_addr = 0;
		// Prepare next iteration
		len -= to_read;
		data += to_read;
		readed += to_read;
		to_read = MIN(len, BLOCK_LEN);
	}

	return readed;
}

static void data_poll(uint16_t addr, uint8_t data)
{
	while ((data & 0x80) != (bus_read(addr) & 0x80));
}

static void byte_program(uint16_t block_addr, uint8_t data)
{
	unlock();
	bus_write(0x5555, 0xA0);
	bus_write(block_addr, data);
	data_poll(block_addr, data);
}

// Write data to current block. block_addr and len must NOT cross
// block boundaries
static void block_write(uint16_t block_addr, const uint8_t *buf, uint16_t len)
{
	block_addr |= 0x8000;
	while (len--) {
		byte_program(block_addr++, *buf++);
	}
}

static int16_t program(uint32_t addr, const uint8_t *data, uint16_t len)
{
	// Current block
	uint8_t cur_block = BLOCK(addr);
	// Address to write within current block
	uint16_t block_addr = addr & BLOCK_MASK;
	// Bytes to write on each iteration, less or equal than block length
	uint16_t to_write = MIN(len, BLOCK_LEN - block_addr);
	uint16_t written = 0;

	while (to_write) {
		block_map(cur_block);
		block_write(block_addr, data, to_write);
		// Move to next block
		cur_block++;
		block_addr = 0;
		// Prepare next iteration
		len -= to_write;
		data += to_write;
		written += to_write;
		to_write = MIN(len, BLOCK_LEN);
	}

	return written;
}

static void erase_unlock(void)
{
	unlock();
	bus_write(0x5555, 0x80);
	bus_write(0x5555, 0xAA);
	bus_write(0x2AAA, 0x55);
}

static int8_t chip_erase(void)
{
	erase_unlock();
	bus_write(0x5555, 0x10);
	data_poll(0, 0xFF);

	return 0;
}

static uint32_t sector_erase(uint32_t addr)
{
	uint16_t block_addr = 0x8000 | (addr & BLOCK_MASK);
	block_map(BLOCK(addr));
	erase_unlock();
	bus_write(block_addr, 0x30);
	data_poll(block_addr, 0xFF);

	// Return address of the next sector
	return ((addr>>SECT_NBITS) + 1)<<SECT_NBITS;
}

// Register Frugal Mapper compatible driver
FLASH_DRV_REGISTER(frugal_map, MDMA_CART_TYPE_FRUGAL_MAPPER)
