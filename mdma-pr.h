/************************************************************************//**
 * \file
 *
 * \author Jesús Alonso (doragasu)
 * \date   2015
 * \defgroup mdma-pr Definitions related to the MDMA protocol implementation.
 * \{
 ****************************************************************************/

#ifndef _MDMA_PR_H_
#define _MDMA_PR_H_

/** \addtogroup mdma-pr MdmaCmds Commands and replies supported by the
 *  platform.
 * \{
 */
enum {
	MDMA_OK = 0,            ///< Used to report OK status during replies
	MDMA_MANID_GET,         ///< Flash chip manufacturer ID request.
	MDMA_DEVID_GET,         ///< Flash chip device ID request.
	MDMA_READ,              ///< Flash data read command.
	MDMA_CART_ERASE,        ///< Cartridge Flash erase command.
	MDMA_SECT_ERASE,        ///< Flash sector erase command.
	MDMA_WRITE,             ///< Flash write (program) command.
	MDMA_MAN_CTRL,          ///< Manual GPIO pin control command.
	MDMA_BOOTLOADER,        ///< Puts board in bootloader mode.
	MDMA_BUTTON_GET,        ///< Gets pushbutton status.
	MDMA_WIFI_CMD,          ///< Command forwarded to the WiFi chip.
	MDMA_WIFI_CMD_LONG,     ///< Long command forwarded to the WiFi chip.
	MDMA_WIFI_CTRL,         ///< WiFi chip control action (using GPIO).
	MDMA_RANGE_ERASE,       ///< Erase a memory range of the flash chip
	MDMA_FEATURES_GET,      ///< Get programmer version and supported features
	MDMA_CART_TYPE_QUERY,   ///< Try guessing the installed cartridge type
	MDMA_CART_TYPE_SET,     ///< Set the cartridge type to use
	MDMA_CART_FLASH_LAYOUT, ///< Get cartridge flash memory layout
	__MDMA_CMD_MAX,		///< Maximum number of commands
	MDMA_ERR = 255          ///< Used to report ERROR during replies.
};
/** \} */

enum {
	MDMA_CART_TYPE_MEGAWIFI = 1,
	MDMA_CART_TYPE_FRUGAL_MAPPER = 2
};

/// Obtains a double word (uint32_t) from the specified variable and offset.
/// This macro is not optimal, but will always work, even if the address is
/// misaligned. Order is little endian
#define MDMA_DWORD_AT(var, pos)		(((uint32_t)((var)[(pos)+3])<<24) | \
		((uint32_t)((var)[(pos)+2])<<16) | \
		((uint32_t)((var)[(pos)+1])<<8)  | \
		(var)[pos])

/// Obtains a 24-bit value (stored as an uint32_t) from the specified
/// variale and offset.
#define MDMA_3BYTES_AT(var, pos)	(((uint32_t)((var)[(pos)+2])<<16) | \
		((uint32_t)((var)[(pos)+1])<<8)  | \
		(var)[pos])

/// Address offset in command request
#define MDMA_ADDR_OFF		4
/// Length offset in command request
#define MDMA_LENGTH_OFF		1

/// Obtains command from a data frame
#define MDMA_CMD(data)		(data[0])
/// Obtains address from a flash read, write or sector erase command
#define MDMA_ADDR(data)			MDMA_3BYTES_AT(data, MDMA_ADDR_OFF)
/// Obtains length from a data read or write command
#define MDMA_LENGTH(data)		MDMA_3BYTES_AT(data, MDMA_LENGTH_OFF)

#endif /*_MDMA_PR_H_*/

/** \} */

