#include <stdlib.h>
#include <string.h>

#include "klist.h"
#include "flash_drv.h"

struct node {
	struct list_head _head;
	const struct flash* flash;
};

static LIST_HEAD(drv_list);
static uint8_t num_drivers = 0;

void flash_drv_add(const struct flash *flash)
{
	struct node *new_node = malloc(sizeof(struct node));
	new_node->flash = flash;

	list_add_tail(&new_node->_head, &drv_list);
	num_drivers++;
}

const struct flash* flash_drv_search(const char *name)
{
	struct node *node;

	list_for_each_entry(node, &drv_list, _head) {
		if (0 == strcmp(name, node->flash->name)) {
			return node->flash;
		}
	}

	return NULL;
}

const struct flash* flash_drv_search_by_key(uint16_t key)
{
	struct node *node;

	list_for_each_entry(node, &drv_list, _head) {
		if (key == node->flash->key) {
			return node->flash;
		}
	}

	return NULL;
}

uint8_t flash_num_drivers_get(void)
{
	return num_drivers;
}

uint8_t flash_keys_get(uint8_t *key_buf, uint8_t buf_len)
{
	struct node *node;
	uint8_t i = 0;

	list_for_each_entry(node, &drv_list, _head) {
		if (i < buf_len) {
			key_buf[i++] = node->flash->key;
		} else {
			goto out;
		}
	}

out:
	return i;
}
