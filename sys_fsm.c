/************************************************************************//**
 * \file
 * \brief System state machine. Receives events from the cartridge and USB
 * interface, and performs the corresponding actions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2015
 ****************************************************************************/
#include "sys_fsm.h"
#include "cart_if.h"
#include "util.h"
#include "bloader.h"
#include "slip.h"
#include "wifi-if.h"
#include "mdma-pr.h"
#include "flash_drv.h"
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Drivers/Board/LEDs.h>
#include <Descriptors.h>
#include <avr/cpufunc.h>
#include <util/delay.h>

/** \addtogroup sys_fsm SfWifiCtrlCode Control code for WiFi module operations.
 *  \{ */
typedef enum {
	SF_WIFI_CTRL_RST = 0,	///< Hold chip in reset state.
	SF_WIFI_CTRL_RUN,	///< Reset the chip.
	SF_WIFI_CTRL_BLOAD,	///< Enter bootloader mode.
	SF_WIFI_CTRL_APP,	///< Start application.
	SF_WIFI_CTRL_SYNC	///< Perform a SYNC attemp.
} SfWifiCtrlCode;
/** \} */

/// Offset for the data payloa of the WiFi command
#define SF_WIFI_CMD_PAYLOAD_OFF		4

/// Maximum number of poll cycles for the UART before timing out
#define SF_WIFI_TOUT_CYCLES_MAX		UINT16_MAX

/// Number of UART poll cycles for WiFi command operations.
//#define SF_WIFI_CMD_TOUT_CYCLES	SF_WIFI_TOUT_CYCLES_MAX
#define SF_WIFI_CMD_TOUT_CYCLES		10000

/** \addtogroup sys_fsm SfSwData Pushbutton data interpretation masks.
 *  \{ */
#define SF_SW_PRESSED	0x01	///< If bit set, button is pressed.
#define SF_SW_EVENT	0x02	///< If bit set, a button event occurred.
/** \} */

/** \addtogroup sys_fsm sf_flags Auxiliar flags to define system status
 * \{
 */
union sf_flags {
	uint8_t all;                 ///< Access to all flags
	struct {
		uint8_t cart_type:2; ///< Configured cartridge type
		uint8_t usb_ready:1; ///< USB attached and ready
	};
};
/** \} */

/** \addtogroup sys_fsm sf_instance Data about the currently running system instance.
 * \{
 */
struct sf_instance {
	const struct flash *flash; ///< Current flash driver
	union sf_flags f;          ///< System status flags
	uint8_t sw;                ///< Switch (pushbutton) status
	uint8_t cart_err;          ///< Cart error or warning
};
/** \} */

/// Command and data bytes for the ESP8266 SYNC command.
static const char sync_frame[] = {
	// 0, cmd, data_len, data_len (16b), chk (32b)
	0x00, 0x08, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00,
	// data
	0x07, 0x07, 0x12, 0x20,
	0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 
	0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 
	0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55,
	0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55
};

/** \addtogroup sys_fsm PortDefs Port definitions for the programmer board.
 * \{ */
enum {
	SF_PORTA = 0,
	SF_PORTB,
	SF_PORTC,
	SF_PORTD,
	SF_PORTE,
	SF_PORTF,
	SF_GPIO_NUM_PORTS
};
/** \} */

/// Maximum length of the write flash payload (in words)
#define SF_MAX_WRITE_WLEN			((VENDOR_O_EPSIZE - 6)/2)
/// Maximum length of the read flash payload (in words)
#define SF_MAX_READ_WLEN			((VENDOR_I_EPSIZE - 6)/2)

/// Buffer for receiving data and sending replies
/// \note Compiler/linker is stupid, and if you place this buffer inside a
/// function body, you will not get an error, not even a single warning,
/// even though the buffer length is larger than the stack. Welcome to 2016!
static uint8_t buf[MAX(VENDOR_O_EPSIZE, VENDOR_I_EPSIZE)];

// System FSM data
static struct sf_instance si;

/************************************************************************//**
 * \brief Module initialization. Must be called before using any other
 * function from this module.
 ****************************************************************************/
void sf_init(void)
{
	// Set default values
	memset(&si, 0, sizeof(struct sf_instance));
}
/************************************************************************//**
 * \brief Initializes megawifi extra hardware
 ****************************************************************************/
static int megawifi_init(void)
{
	si.cart_err = UartInit();

	return si.cart_err;
}

/************************************************************************//**
 * \brief Receive a complete endpoint data frame.
 *
 * \param[out] data Array containing the received data.
 ****************************************************************************/
static void data_recv(uint8_t *data)
{
	// We do not need to select endpoint, as it has been previously
	// selected to check if there is incoming data
	Endpoint_Read_Stream_LE(data, VENDOR_O_EPSIZE, NULL);
	Endpoint_ClearOUT();
}

/************************************************************************//**
 * \brief Send a complete endpoint data frame.
 *
 * \param[in] data Array with the data to send.
 * \param[in] len  Number of bytes of data to send.
 ****************************************************************************/
static void data_send(uint8_t *data, uint16_t len)
{
	Endpoint_SelectEndpoint(VENDOR_IN_EPADDR);
	Endpoint_Write_Stream_LE(data, len, NULL);
	Endpoint_ClearIN();
}

/************************************************************************//**
 * \brief Read/write to GPIO pins. Input/output parameters take a byte for
 * each port from PORTA to PORTF.
 *
 * \param[in] mask	Array with the pin mask. Pins set will be readed/written
 * \param[in] r_w	Read/write mask. '1' reads, '0' writes.
 * \param[in] value	Only used for writes. Holds the value to write.
 * \param[out] readed Values read for specified pins.
 *
 * \todo Write this function in a more elegant way!
 ****************************************************************************/
static void gpio_action(uint8_t *mask, uint8_t *r_w, uint8_t *value,
		uint8_t *readed)
{
	uint8_t scratch;

	// First perform read on requested ports
	if ((scratch = mask[SF_PORTA] & r_w[SF_PORTA])) {
		// Configure pins for reading and obtain value
		DDRA &= ~scratch;	// Set pins as input
		PORTA |= scratch;	// Enable pullups
		readed[SF_PORTA] = PINA & scratch;	// Read data
	}
	if ((scratch = mask[SF_PORTB] & r_w[SF_PORTB])) {
		DDRB &= ~scratch;
		PORTB |= scratch;
		readed[SF_PORTB] = PINB & scratch;
	}
	if ((scratch = mask[SF_PORTC] & r_w[SF_PORTC])) {
		DDRC &= ~scratch;
		PORTC |= scratch;
		readed[SF_PORTC] = PINC & scratch;
	}
	if ((scratch = mask[SF_PORTD] & r_w[SF_PORTD])) {
		DDRD &= ~scratch;
		PORTD |= scratch;
		readed[SF_PORTD] = PIND & scratch;
	}
	if ((scratch = mask[SF_PORTE] & r_w[SF_PORTE])) {
		DDRE &= ~scratch;
		PORTE |= scratch;
		readed[SF_PORTE] = PINE & scratch;
	}
	if ((scratch = mask[SF_PORTF] & r_w[SF_PORTF])) {
		DDRF &= ~scratch;
		PORTF |= scratch;
		readed[SF_PORTF] = PINF & scratch;
	}
	// Write to requested pins
	if ((scratch = mask[SF_PORTA] & ~r_w[SF_PORTA])) {
		// Configure pins for writing and write value
		DDRA |= scratch;	// Set pins as outputs
		PORTA |= scratch & value[SF_PORTA];		// Write ones
		PORTA &= ~(scratch & ~value[SF_PORTA]);	// Write zeros
	}
	if ((scratch = mask[SF_PORTB] & ~r_w[SF_PORTB])) {
		DDRB |= scratch;
		PORTB |= scratch & value[SF_PORTB];
		PORTB &= ~(scratch & ~value[SF_PORTB]);
	}
	if ((scratch = mask[SF_PORTC] & ~r_w[SF_PORTC])) {
		DDRC |= scratch;
		PORTC |= scratch & value[SF_PORTC];
		PORTC &= ~(scratch & ~value[SF_PORTC]);
	}
	if ((scratch = mask[SF_PORTD] & ~r_w[SF_PORTD])) {
		DDRD |= scratch;
		PORTD |= scratch & value[SF_PORTD];
		PORTD &= ~(scratch & ~value[SF_PORTD]);
	}
	if ((scratch = mask[SF_PORTE] & ~r_w[SF_PORTE])) {
		DDRE |= scratch;
		PORTE |= scratch & value[SF_PORTE];
		PORTE &= ~(scratch & ~value[SF_PORTE]);
	}
	if ((scratch = mask[SF_PORTF] & ~r_w[SF_PORTF])) {
		DDRF |= scratch;
		PORTF |= scratch & value[SF_PORTF];
		PORTF &= ~(scratch & ~value[SF_PORTF]);
	}
}

static uint16_t mdma_wifi_cmd(uint8_t *data)
{
	uint16_t len = data[1];
	uint8_t cmd = data[5];
	// Forward command to WiFi module and read response
	if (SlipFrameSendPoll(data + SF_WIFI_CMD_PAYLOAD_OFF, len,
				SF_WIFI_CMD_TOUT_CYCLES) != len) {
		data[0] = MDMA_ERR;
		data[1] = 1;
		return 2;
	}
	// Read module response
	for (uint8_t step = 100; step; step--) {
		if (!SlipFrameRecvPoll(data, VENDOR_O_EPSIZE, &len,
					SF_WIFI_CMD_TOUT_CYCLES)) {
			if (1 == data[0] && data[1] == cmd) {
				/// \todo FIXME should also check status and error
				/// fields (offsets 8 and 9).
				data[0] = MDMA_OK;
				return len;
			}
		}
		USB_USBTask();
	}
	return 2;
}

static uint16_t mdma_wifi_cmd_long(uint8_t *data)
{
	uint16_t step;
	uint16_t len = data[1] | (data[2]<<8);

	// Forward split long command to WiFi module and read response.
	// Data is received split in several bulk transfers until
	// completion
	SlipSplitFrameSendSof(SF_WIFI_CMD_TOUT_CYCLES);
	Endpoint_SelectEndpoint(VENDOR_OUT_EPADDR);
	for (uint16_t sent = 0; sent < len; sent += step) {
		data_recv(data);
		step = MIN(VENDOR_O_EPSIZE, len - sent);
		if (SlipSplitFrameAppendPoll(data, step, SF_WIFI_CMD_TOUT_CYCLES) !=
				step) {
			data[0] = MDMA_ERR;
			return 1;
		}
	}
	SlipSplitFrameSendEof(SF_WIFI_CMD_TOUT_CYCLES);
	// Completed, receive module response
	if (SlipFrameRecvPoll(data, VENDOR_O_EPSIZE, &len,
				SF_WIFI_CMD_TOUT_CYCLES)) {
		data[0] = MDMA_ERR;
		return 1;
	}
	return len; // OK!
}

static void sf_wifi_ctrl_sync(uint8_t *data)
{
	uint16_t len;

	// Send the SYNC frame and try reading the response
	// until success or too many attemps.
	for (uint8_t step = data[2]; step; step--) {
		UartFlush();
		SlipFrameSendPoll((uint8_t*)sync_frame, sizeof(sync_frame),
				SF_WIFI_CMD_TOUT_CYCLES);
		while(!UartTxFifoEmpty());
		if (!SlipFrameRecvPoll(data, VENDOR_O_EPSIZE, &len,
					SF_WIFI_CMD_TOUT_CYCLES)) {
			// Check we received the sync response
			if (1 == data[0] && 8 == data[1]) {
				data[0] = MDMA_OK;
				return;
			}
		}
		// Avoid USB timing out
		USB_USBTask();
	}
	// Retries completed before sync correct
	data[0] = MDMA_ERR;
}

static uint16_t mdma_wifi_ctrl(uint8_t *data)
{

	switch (data[1]) {
	case SF_WIFI_CTRL_RST:
		// Put module in reset
		WiFiReset();
		break;

	case SF_WIFI_CTRL_RUN:
		// Release reset
		WiFiStart();
		break;

	case SF_WIFI_CTRL_BLOAD:
		// Set bootloader mode
		WiFiPrgEnable();
		break;

	case SF_WIFI_CTRL_APP:
		// Set application mode
		WiFiPrgDisable();
		break;

	case SF_WIFI_CTRL_SYNC:
		sf_wifi_ctrl_sync(data);
		// Return early to avoid overwriting result set by function
		return 1;

	default:
		// Unsupported!!!
		data[0] = MDMA_ERR;
		return 1;
	}

	data[0] = MDMA_OK;
	return 1;
}

/************************************************************************//**
 * \brief Process a WiFi module related command.
 *
 * \param[in] event Command directed to the WiFi chip.
 * \param[inout] data Array with the data containing the command request,
 *                    and the command reply once the function returns.
 ****************************************************************************/
static uint16_t wifi_cmd_proc(uint8_t event, uint8_t *data)
{
	// Check we have a command request (because we received data).
	if (SF_EVT_DIN != event) {
		return 0;
	}

	UartFlush();

	// Check which command we have in.
	switch (MDMA_CMD(data)) {
	case MDMA_WIFI_CMD:		// Forward command to the WiFi module
		return mdma_wifi_cmd(data);

	case MDMA_WIFI_CMD_LONG:	// Forward long command to WiFi module
		return mdma_wifi_cmd_long(data);

	case MDMA_WIFI_CTRL:		// WiFi module control using GPIO
		return mdma_wifi_ctrl(data);

	default:
		break;
	}
	// Unsupported!!!
	data[0] = MDMA_ERR;
	return 1;
}

static void mdma_read(uint8_t *data)
{
	uint32_t addr = MDMA_ADDR(data);
	uint32_t length = MDMA_LENGTH(data);
	// Send OK
	data[0] = MDMA_OK;
	data_send(data, 1);

	// Data send loop
	while (length) {
		const uint16_t step = MIN(length, VENDOR_I_EPSIZE);
		si.flash->read(addr, data, step);
		data_send(data, step);
		addr += step;
		length -= step;
	}
}

static void mdma_write(uint8_t *data)
{
	uint32_t addr = MDMA_ADDR(data);
	uint32_t length = MDMA_LENGTH(data);

	// Send OK and enter write loop
	data[0] = MDMA_OK;
	data_send(data, 1);

	// Data write loop
	Endpoint_SelectEndpoint(VENDOR_OUT_EPADDR);
	while (length) {
		// Read data
		data_recv(data);
		// Data received on endpoint
		const uint16_t step = MIN(length, VENDOR_O_EPSIZE);
		const uint16_t written = si.flash->program(addr, data, step);
		if (written != step) {
			// Note: on error routine stops writing, and thus host
			// will error due to a write timeout
			break;
		}
		addr += step;
		length -= step;
	}
}

static uint16_t mdma_man_ctrl(uint8_t *data)
{
	uint8_t port[SF_GPIO_NUM_PORTS];
	uint16_t rep_len;

	// Check magic bytes
	if ((data[1] == 0x19) && (data[2] == 0x85) &&
			(data[3] == 0xBA) && (data[4] == 0xDA) &&
			(data[5] == 0x55)) {
		gpio_action(&data[6], &data[12], &data[18], port);
		rep_len = 1 + SF_GPIO_NUM_PORTS;
	} else {
		// Incorrect magic bytes, return error
		data[0] = MDMA_ERR;
		rep_len = 1;
	}

	return rep_len;
}

static int8_t flash_range_erase(uint32_t addr, uint32_t length)
{
	const uint32_t limit = addr + length - 1;
	uint32_t pos = addr;

	while (pos <= limit) {
		const uint32_t next = si.flash->sector_erase(pos);
		if (!next) {
			// Sector erase failed
			return 1;
		}
		pos = next;
	}

	return 0;
}

static uint16_t mdma_range_erase(uint8_t *data)
{
	// Unpack address and length
	uint32_t addr = MDMA_3BYTES_AT(data, 1);
	uint32_t length = MDMA_DWORD_AT(data, 4);

	// Issue erase command
	data[0] = flash_range_erase(addr, length) ? MDMA_ERR : MDMA_OK;

	return 1;
}

static uint16_t feats_get(uint8_t *data)
{
	int16_t i = 0;
	data[i++] = MDMA_OK;
	data[i++] = SF_VER_MAJOR;
	data[i++] = SF_VER_MINOR;
	data[i++] = SF_VER_MICRO;
	data[i++] = flash_num_drivers_get();
	i += flash_keys_get(&data[i], sizeof(buf) - i);

	return i;
}

static uint16_t cart_flash_layout_get(uint8_t *data)
{
	const struct flash_layout *layout;

	if (!si.flash) {
		// Driver not initialized
		data[0] = MDMA_ERR;
		return 1;
	}

	layout = si.flash->layout_get();
	if (!layout) {
		data[0] = MDMA_ERR;
		return 1;
	}

	data[0] = MDMA_OK;
	data[1] = 0;
	data[2] = 0;
	data[3] = sizeof(struct flash_layout);
	// TODO: should go field by field for data to be bit endian, but yeah,
	// I'm lazy so just memcpy the thing
	memcpy(&data[4], layout, data[3]);

	return 4 + data[3];
}

static void flash_deinit(void)
{
	if (si.flash) {
		si.flash->deinit();
		si.flash = NULL;
	}
}

static void cart_type_set(uint8_t *data)
{
	flash_deinit();
	const struct flash *flash = flash_drv_search_by_key(data[1]);

	if (flash && 0 == flash->init()) {
		if (MDMA_CART_TYPE_MEGAWIFI == flash->key) {
			megawifi_init();
		}
		si.flash = flash;
		data[0] = MDMA_OK;
	} else {
		data[0] = MDMA_ERR;
	}
}

static uint16_t dev_id_get(uint8_t *data)
{
	data[0] = MDMA_OK;
	uint8_t id_len;
	const uint8_t *dev_id = si.flash->dev_id_get(&id_len);
	data[1] = id_len;
	memcpy(&data[2], dev_id, id_len);

	return 2 + data[1];
}

/************************************************************************//**
 * \brief Processes a command, doing the requested action, and preparing the
 * reply to be sent.
 *
 * \param[inout] data Incoming data containing the command. On function return,
 *               it contains the reply to send to the command.
 * \return The number of bytes of the reply to be sent.
 ****************************************************************************/
static uint16_t cmd_proc(uint8_t *data)
{
	// Length of the reply that must be sent to host
	uint16_t rep_len = 0;
	uint8_t cmd = MDMA_CMD(data);

	if ((cmd >= MDMA_MANID_GET && cmd <= MDMA_WRITE) ||
			(cmd >= MDMA_WIFI_CMD && cmd <= MDMA_RANGE_ERASE)) {
		// These commands require the cart to be initialized
		if (!si.flash) {
			data[0] = MDMA_ERR;
			return 1;
		}
	}

	switch (MDMA_CMD(data)) {
	case MDMA_MANID_GET:	// Flash manufacturer ID
		data[0] = MDMA_OK;
		data[1] = si.flash->man_id_get();
		rep_len = 2;
		break;

	case MDMA_DEVID_GET:	// Flash device ID
		rep_len = dev_id_get(data);
		break;

	case MDMA_READ:		// Flash read
		mdma_read(data);
		break;

	case MDMA_CART_ERASE:	// Complete flash erase
		data[0] = si.flash->chip_erase() ? MDMA_ERR : MDMA_OK;
		rep_len = 1;
		break;

	case MDMA_SECT_ERASE:	// Complete flash sector erase
		data[0] = si.flash->sector_erase(MDMA_DWORD_AT(data, 1)) ?
			MDMA_OK : MDMA_ERR;
		rep_len = 1;
		break;

	case MDMA_WRITE:	// Flash write
		mdma_write(data);
		break;

	case MDMA_MAN_CTRL:	// Manual line control
		rep_len = mdma_man_ctrl(data);
		break;

	case MDMA_BUTTON_GET:	// Read button status
				// Return button status and clear button events
		rep_len = 2;
		data[0] = MDMA_OK;
		data[1] = si.sw;
		si.sw &= ~SF_SW_EVENT;
		break;

	case MDMA_BOOTLOADER:	// Enter bootloader
		JumpToBootloader();
		// The function above does never return

		// WiFi module related commands, processed in a separate function.
	case MDMA_WIFI_CMD:
	case MDMA_WIFI_CMD_LONG:
	case MDMA_WIFI_CTRL:
		rep_len = wifi_cmd_proc(SF_EVT_DIN, data);
		break;

	case MDMA_RANGE_ERASE:
		rep_len = mdma_range_erase(data);
		break;

	case MDMA_CART_TYPE_QUERY:
		// Unsupported
		data[0] = MDMA_ERR;
		rep_len = 1;
		break;

	case MDMA_CART_TYPE_SET:
		cart_type_set(data);
		rep_len = 1;
		break;

	case MDMA_FEATURES_GET:
		rep_len = feats_get(data);
		break;

	case MDMA_CART_FLASH_LAYOUT:
		rep_len = cart_flash_layout_get(data);
		break;

	default:
		// Unsupported command, return error
		data[0] = MDMA_ERR;
		rep_len = 1;
		break;
	}
	return rep_len;
}

/************************************************************************//**
 * \brief Puts cart pins at their default (idle bus) state.
 ****************************************************************************/
static void cart_remove(void)
{
	CIF_CLR__RST;
	CIF_SET__TIME;
	flash_deinit();
}

/************************************************************************//**
 * \brief Takes an incoming event and executes a cycle of the system FSM
 *
 * \param[in] evt Incoming event to be processed.
 *
 * \note Lots of states have been removed, might be needed if problems arise
 * because USB_USBTask() needs to be serviced more often than it is with
 * the current implementation.
 ****************************************************************************/
void sf_fsm_cycle(enum sf_event evt)
{
	// Holds reply length
	uint16_t rep_len;

	// Process prioritary events  and events that can generate more
	// events (like data reception from host).
	switch (evt) {
	case SF_EVT_USB_ATT:		// USB attached and enumerated
		si.f.usb_ready = TRUE;
		break;

	case SF_EVT_USB_DET:		// USB detached
	case SF_EVT_USB_ERR:		// Error on USB interface
		si.f.usb_ready = FALSE;
		cart_remove();
		break;

	case SF_EVT_DIN:
		// Get data from USB endpoint and process command
		data_recv(buf);
		rep_len = cmd_proc(buf);
		// Send reply to command if any
		if (rep_len) data_send(buf, rep_len);
		break;

	case SF_EVT_SW_PRESS:		// Button pressed event
		si.sw = SF_SW_EVENT | SF_SW_PRESSED;
		break;

	case SF_EVT_SW_REL:			// Button released event
		si.sw = SF_SW_EVENT;
		break;

	default:
		break;
	}
}

