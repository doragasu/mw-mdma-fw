#ifndef __FLASH_DRV_H__
#define __FLASH_DRV_H__

#include <stdint.h>

/// Maximum number of regions in flash chip, per CFI definition
#define FLASH_REGION_MAX 4

/// Metadata of a flash region, consisting of several sectors.
struct flash_region {
	uint32_t start_addr;    //< Sector start address
	uint16_t num_sectors;   //< Number of sectors
	uint16_t sector_len;	//< Sector length in 256 byte units
};

/// Metadata of a flash chip, describing memory layout.
 struct flash_layout {
	uint32_t len;                  //< Length of chip in bytes
	uint16_t num_regions;          //< Number of regions in chip
	struct flash_region region[FLASH_REGION_MAX]; //< Region data array
}__attribute__((packed));

typedef int8_t (*flash_init_cb)(void);
typedef void (*flash_deinit_cb)(void);
typedef uint8_t (*flash_man_id_get_cb)(void);
typedef const struct flash_layout* (*flash_layout_get_cb)(void);
typedef const uint8_t* (*flash_dev_id_get_cb)(uint8_t *id_len);
typedef int16_t (*flash_read_cb)(uint32_t addr, uint8_t *data, uint16_t len);
typedef int16_t (*flash_program_cb)(uint32_t addr, const uint8_t *data, uint16_t len);
typedef int8_t (*flash_chip_erase_cb)(void);
typedef uint32_t (*flash_sector_erase_cb)(uint32_t addr);

struct flash {
	const char *name;
	uint16_t key;
	flash_init_cb init;
	flash_deinit_cb deinit;
	flash_man_id_get_cb man_id_get;
	flash_dev_id_get_cb dev_id_get;
	flash_layout_get_cb layout_get;
	flash_read_cb read;
	flash_program_cb program;
	flash_chip_erase_cb chip_erase;
	flash_sector_erase_cb sector_erase;
};

void flash_drv_add(const struct flash *flash);
const struct flash* flash_drv_search_by_name(const char *name);
const struct flash* flash_drv_search_by_key(uint16_t key);
uint8_t flash_num_drivers_get(void);
uint8_t flash_keys_get(uint8_t *key_buf, uint8_t buf_len);

/// Registers a flash driver with a specific name and key
#define FLASH_DRV_REGISTER(drv_name, drv_key) \
	static const struct flash drv_name ## _drv = { \
		.name = #drv_name, \
		.key = drv_key, \
		.init = init, \
		.deinit = deinit, \
		.man_id_get = man_id_get, \
		.dev_id_get = dev_id_get, \
		.layout_get = layout_get, \
		.read = read, \
		.program = program, \
		.chip_erase = chip_erase, \
		.sector_erase = sector_erase \
	};\
	static void __attribute__((constructor)) \
	flash_drv_ ## drv_name ## _add(void) { flash_drv_add(&drv_name ## _drv); }

#endif
