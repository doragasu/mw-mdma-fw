/************************************************************************//**
 * \file
 * \brief System state machine. Receives events from the cartridge and USB
 * interface, and performs the corresponding actions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2015
 * \defgroup sys_fsm System finite state machine.
 * \{
 ****************************************************************************/

#ifndef _SYS_FSM_H_
#define _SYS_FSM_H_

/// Major version
#define SF_VER_MAJOR 1
/// Minor version
#define SF_VER_MINOR 2
/// Micro version
#define SF_VER_MICRO 1

/** \addtogroup sys_fsm SfEvents State machine events.
 * These are the events that the state machine understands and can process.
 * \{ */
enum __attribute__((__packed__)) sf_event {
	SF_EVT_NONE = 0, ///< No event (just cycle FSM)
	SF_EVT_USB_ATT,  ///< USB attached and enumerated
	SF_EVT_USB_DET,  ///< USB detached
	SF_EVT_USB_ERR,  ///< Error on USB interface
	SF_EVT_DIN,      ///< Data reception from host
	SF_EVT_DOUT,     ///< Data sent to host
	SF_EVT_SW_PRESS, ///< Button pressed
	SF_EVT_SW_REL,   ///< Button released
	__SF_EVT_MAX     ///< Maximum number of events
};
/** \} */

/************************************************************************//**
 * \brief Module initialization. Must be called before using any other function
 * from this module.
 ****************************************************************************/
void sf_init(void);

/************************************************************************//**
 * \brief Takes an incoming event and executes a cycle of the system FSM
 *
 * \param[in] evt Incoming event to be processed.
 *
 * \note Lots of states have been removed, might be needed if problems arise
 * because USB_USBTask() needs to be serviced more often than it is with
 * the current implementation.
 ****************************************************************************/
void sf_fsm_cycle(enum sf_event evt);

#endif /*_SYS_FSM_H_*/

/** \} */

